import java.util.Iterator;
import java.util.Scanner;

public class AddTwoMatrices {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the row of the matrix (r - c): ");
		System.out.println("Enter the number of rows: ");
		int row = sc.nextInt();
		System.out.println("Enter the number of columns: ");
		int column = sc.nextInt();
		int[][] matrix1 = new int[row][column];
		int[][] matrix2 = new int[row][column];
		System.out.println("Enter all elements of the first matrix: ");
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.printf("Enter [%d][%d]: ", i + 1, j + 1);
				matrix1[i][j] = sc.nextInt();
			}
		}
		System.out.println("Enter all elements of the second matrix: ");
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.printf("Enter [%d][%d]: ", i + 1, j + 1);
				matrix2[i][j] = sc.nextInt();
			}
		}
		System.out.println("Sum of 2 entered matrices: ");
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				System.out.print((matrix1[i][j] + matrix2[i][j]) + " ");
			}
			System.out.println();
		}

	}
}
