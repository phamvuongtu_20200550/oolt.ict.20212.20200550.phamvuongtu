import java.util.Scanner;

public class SortArray {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int size = 0;
		do {
			System.out.println("Enter the size of the array: ");
			size = sc.nextInt();
		} while (size < 0);
		int[] arr = new int[size];
		System.out.println("Enter the value of each elements: ");
		for (int i = 0; i < size; i++) {
			System.out.printf("Enter the value of element %d: ", i + 1);
			arr[i] = sc.nextInt();
		}
		System.out.println("Before sorting: ");
		for (int i = 0; i < size; i++) {
			System.out.print(arr[i] + " ");
		}
		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				if (arr[i] > arr[j]) {
					int tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;
				}
			}
		}
		int sum = 0;
		System.out.println("\nAfter sorting: ");
		for (int i = 0; i < size; i++) {
			System.out.print(arr[i] + " ");
			sum += arr[i];
		}
		System.out.printf("\nSum = %d\n", sum);
		System.out.println("Average value = " + (Math.round((double) sum * 100 / size)) / 100.0);
	}
}
