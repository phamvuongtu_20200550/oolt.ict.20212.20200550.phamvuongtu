import java.util.Scanner;

public class DaysOfAMonth {
	public static int checkMonth(String month) {
		int m;
		if (month.equals("January") || month.equals("Jan.") || month.equals("Jan") || month.equals("1")) {
			return m = 1;
		}
		if (month.equals("Febuary") || month.equals("Feb.") || month.equals("Feb") || month.equals("2")) {
			return m = 2;
		}
		if (month.equals("April") || month.equals("Apr.") || month.equals("Apr") || month.equals("4")) {
			return m = 4;
		}
		if (month.equals("May") || month.equals("5")) {
			return m = 5;
		}
		if (month.equals("June") || month.equals("6")) {
			return m = 6;
		}
		if (month.equals("July") || month.equals("7")) {
			return m = 7;
		}
		if (month.equals("August") || month.equals("Aug.") || month.equals("Aug") || month.equals("8")) {
			return m = 8;
		}
		if (month.equals("September") || month.equals("Sep.") || month.equals("Sep") || month.equals("9")) {
			return m = 9;
		}
		if (month.equals("October") || month.equals("Oct.") || month.equals("Oct") || month.equals("10")) {
			return m = 10;
		}
		if (month.equals("November") || month.equals("Nov.") || month.equals("Nov") || month.equals("11")) {
			return m = 11;
		}
		if (month.equals("December") || month.equals("Dec.") || month.equals("Dec") || month.equals("12")) {
			return m = 12;
		}
		return -1;
	}

	public static int isLeapYear(int year) {
		if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
			return 29;
		} else {
			return 28;
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String month;
		int m;
		int year;
		int days = 0;
		do {
			System.out.println("Enter a month (in full name, abbreviation, in 3 letters, or in number).");
			month = sc.nextLine();
			m = checkMonth(month);
		} while (m < 0);
		do {
			System.out.println("Enter a year (in a non-negative number and enter all the digits).");
			year = sc.nextInt();
		} while (year < 0);
		switch (m) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			days = 31;
			break;
		case 2:
			days = isLeapYear(year);
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			days = 30;
			break;
		}
		System.out.printf("%s of %d has %d days", month, year, days);
	}
}
