import java.util.Iterator;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private int qtyOrdered = 0;

	private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

	public int qtyOrdered() {
		return qtyOrdered;
	}

	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered < MAX_NUMBERS_ORDERED && qtyOrdered >= 0) {
			itemOrdered[qtyOrdered] = disc;
			qtyOrdered++;
			System.out.println("The disc '" + disc.getTitle() + "' has been added.");
			if (qtyOrdered == MAX_NUMBERS_ORDERED - 1)
				System.out.println("The cart is almost full.");
			if (qtyOrdered == MAX_NUMBERS_ORDERED)
				System.out.println("The cart is full.");
		} else {
			System.out.println("Can't add the disc '" + disc.getTitle() + "' into the cart. The cart is already full!");
		}
	}

	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int pos = -1;
		for (int i = 0; i < qtyOrdered; i++) {
			if (itemOrdered[i] == disc) {
				pos = i;
			}
//			System.out.println(pos);
		}
		if (pos >= 0) {
			System.out.println("The disc '" + disc.getTitle() + "' has been deleted!");
			for (int i = pos; i < qtyOrdered - 1; i++) {
				itemOrdered[i] = itemOrdered[i + 1];
			}
			qtyOrdered--;
		} else {
			System.out.println("Cannot delete the disc '" + disc.getTitle() + "'");
		}
	}

	public float totalCost() {
		float sum = 0f;
		for (int i = 0; i < qtyOrdered; i++) {
			sum += itemOrdered[i].getCost();
		}
		return sum;
	}
}
