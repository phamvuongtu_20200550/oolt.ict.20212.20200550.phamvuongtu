import java.time.LocalDateTime;
import java.util.Scanner;

public class MyDate {
	private int day, month, year;

	String[] monthList = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	public boolean isValidDate(int day, int month, int year) {
		if (year < 0) {
			return false;
		}
		if (month < 1 && month > 12) {
			return false;
		}
		switch (month) {
		case 1, 3, 5, 7, 8, 10, 12:
			if (day >= 1 && day <= 31) {
				return true;
			} else {
				return false;
			}
		case 4, 6, 9, 11:
			if (day >= 1 && day <= 30) {
				return true;
			} else {
				return false;
			}
		case 2:
			if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
				if (day >= 1 && day <= 29) {
					return true;
				}
			} else if (day >= 1 && day <= 28) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	public MyDate() {
		this.day = LocalDateTime.now().getDayOfMonth();
		this.month = LocalDateTime.now().getMonthValue();
		this.year = LocalDateTime.now().getYear();
	}

	public MyDate(int day, int month, int year) {
		if (isValidDate(day, month, year)) {
			this.day = day;
			this.month = month;
			this.year = year;
			System.out.println("Create a new date successfully");
		} else {
			System.out.println("Invalid date");
		}
	}

	public MyDate(String date) {
		int day = stringToDay(date);
		int month = stringToMonth(date);
		int year = stringToYear(date);
		if (isValidDate(day, month, year)) {
			this.day = day;
			this.month = month;
			this.year = year;
			System.out.println("Create a new date successfully");
		} else {
			System.out.println("Invalid date");
		}
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public int stringToDay(String date) {
		String[] value = date.split(" ");
		String tmp = value[1].substring(0, 2);
		if (isInteger(tmp)) {
			return Integer.parseInt(tmp);
		} else {
			tmp = value[1].substring(0, 1);
			return Integer.parseInt(tmp);
		}
	}

	public int stringToMonth(String date) {
		String[] value = date.split(" ");
		String tmp = value[0].substring(0, 3);
		for (int i = 0; i < monthList.length; i++) {
			if (tmp.equals(monthList[i])) {
				return i + 1;
			}
		}
		return -1;
	}

	public int stringToYear(String date) {
		String[] value = date.split(" ");
		return Integer.parseInt(value[2]);
	}

	public void accept() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter date in the form: Month(Full name) date year: ");
		String date = sc.nextLine();
		int day = stringToDay(date);
		int month = stringToMonth(date);
		int year = stringToYear(date);
		if (isValidDate(day, month, year)) {
			this.day = day;
			this.month = month;
			this.year = year;
			System.out.println("Create a new date successfully");
		} else {
			System.out.println("Invalid date");
		}
	}

	public void print() {
		System.out.println(this.getDay() + "/" + this.getMonth() + "/" + this.getYear());
	}

	public static void main(String[] args) {
		System.out.println("Date 1: ");
		MyDate md1 = new MyDate();
		md1.print();

		System.out.println("Date 2: ");
		MyDate md2 = new MyDate(15, 2, 2002);
		md2.print();

		System.out.println("Date 3: ");
		MyDate md3 = new MyDate(32, 2, 2002);
		md3.print();

		System.out.println("Date 4: ");
		MyDate md4 = new MyDate(15, 14, 2002);
		md4.print();

		System.out.println("Date 5: ");
		MyDate md5 = new MyDate("February 15th 2002");
		md5.print();
		md5.accept();
		md5.print();

	}

}
