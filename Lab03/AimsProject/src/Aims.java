
public class Aims {

	public static void main(String[] args) {
		Order anOrder = new Order();

		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);

		anOrder.addDigitalVideoDisc(dvd1);

		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("Geogre Lucas");
		dvd2.setLength(124);

		anOrder.addDigitalVideoDisc(dvd2);

		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Alladin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);

		anOrder.addDigitalVideoDisc(dvd3);

		System.out.println("The total cost is: " + anOrder.totalCost());

		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Oggy");
		dvd4.setCategory("Animation");
		dvd4.setCost(20.1f);
		dvd4.setDirector("Pham Vuong Tu");
		dvd4.setLength(60);

		anOrder.addDigitalVideoDisc(dvd4);
		System.out.println("The total cost is: " + anOrder.totalCost());

		anOrder.removeDigitalVideoDisc(dvd1);
		System.out.println("The total cost is: " + anOrder.totalCost());

		DigitalVideoDisc dvd5 = new DigitalVideoDisc("Harry Potter and the Deathly Hallows II", "Fantasy",
				"David Yates", 175, 26.5f);
		anOrder.addDigitalVideoDisc(dvd5);

		DigitalVideoDisc dvd6 = new DigitalVideoDisc("Harry Potter and the Order of Phoenix", "Fantasy", "David Yates",
				147, 24.5f);
		anOrder.addDigitalVideoDisc(dvd6);

		DigitalVideoDisc dvd7 = new DigitalVideoDisc("Avatar", "Science Fiction", "Tu Pham", 70, 34.5f);
		anOrder.addDigitalVideoDisc(dvd7);

		DigitalVideoDisc dvd8 = new DigitalVideoDisc("Titanic", "Romantic", "Hung", 120, 14.90f);
		anOrder.addDigitalVideoDisc(dvd8);
		System.out.println("The total cost is: " + anOrder.totalCost());
		anOrder.removeDigitalVideoDisc(dvd5);
		System.out.println("The total cost is: " + anOrder.totalCost());

	}

}
