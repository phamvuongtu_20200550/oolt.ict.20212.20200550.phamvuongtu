import java.util.Scanner;

// Program to solve the second degree equation with one variable
public class SolveSecondDegreeEquation {

    public static void SolveFirstDegreeEquation(double a, double b) {
        if (a == 0) {
            if (b == 0) {
                System.out.println("The equation has infinitely solutions\n");
            } else
                System.out.println("The equation a no solution\n");
        } else
            System.out.println("The equation has a unique solution x = " + Math.round(-b * 100 / a) / 100.0);
    }

    public static void SolveProblem(double a, double b, double c) {
        if (a == 0) {
            SolveFirstDegreeEquation(b, c);
        } else {
            double delta = Math.pow(b, 2) - 4 * a * c;
            if (delta == 0) {
                System.out.println("The equation has double root " + -b / (2 * a));
            } else if (delta > 0) {
                double x1 = (-b - Math.sqrt(delta)) / (2 * a);
                double x2 = (-b + Math.sqrt(delta)) / (2 * a);
                System.out.println("The equation has two distinct roots " + x1 + " and " + x2);
            } else {
                System.out.println("The equation has no solution");
            }
        }
    }

    public static void main(String[] args) {
        double a, b, c;
        Scanner sc = new Scanner(System.in);
        a = sc.nextDouble();
        b = sc.nextDouble();
        c = sc.nextDouble();
        SolveProblem(a, b, c);
    }
}
