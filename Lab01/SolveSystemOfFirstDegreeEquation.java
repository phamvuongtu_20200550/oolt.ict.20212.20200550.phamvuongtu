import java.util.Scanner;

// Program to solve the system of first degree equation with two variables

public class SolveSystemOfFirstDegreeEquation {
    public static void main(String[] args) {
        double a1, b1, c1, a2, b2, c2;
        Scanner sc = new Scanner(System.in);
        a1 = sc.nextDouble();
        b1 = sc.nextDouble();
        c1 = sc.nextDouble();
        a2 = sc.nextDouble();
        b2 = sc.nextDouble();
        c2 = sc.nextDouble();
        double d = a1 * b2 - a2 * b1;
        double d1 = c1 * b2 - c2 * b1;
        double d2 = a1 * c2 - a2 * c1;
        if (d != 0) {
            System.out.println("The system has a unique solution (x1, x2) = " + "(" + d1 / d + ", " + d2 / d + ")");
        } else {
            if (d1 == 0 && d2 == 0) {
                System.out.println("The system has infinitely many solutions");
            } else
                System.out.println("The system has no solution");
        }
    }
}
