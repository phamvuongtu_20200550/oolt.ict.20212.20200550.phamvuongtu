// Program to solve the first degree equation with one variable

import java.util.Scanner;

public class SolveFirstDegreeEquation {
    public static void main(String[] args) {
        double a, b;
        Scanner sc = new Scanner(System.in);
        a = sc.nextDouble();
        b = sc.nextDouble();
        if (a == 0) {
            if (b == 0) {
                System.out.println("The equation has infinitely solutions\n");
            } else
                System.out.println("The equation a no solution\n");
        } else
            System.out.println("The equation has a unique solution x = " + Math.round(-b * 100 / a) / 100.0);
    }
}
