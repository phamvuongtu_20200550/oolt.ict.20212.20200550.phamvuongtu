import javax.swing.JOptionPane;

/**
 * Problem225
 */
public class Problem225 {
    public static void main(String[] args) {
        String strNum1, strNum2;
        String strNotification = "Result: ";
        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number",
                JOptionPane.INFORMATION_MESSAGE);

        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number: ",
                JOptionPane.INFORMATION_MESSAGE);
        double num1 = Double.parseDouble(strNum1);
        double num2 = Double.parseDouble(strNum2);
        if (num2 == 0) {
            JOptionPane.showMessageDialog(null, "Invalid input data: Second number is 0", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        strNotification += "\n";
        strNotification += strNum1 + " + " + strNum2 + " = " + (num1 + num2) + "\n";
        strNotification += strNum1 + " - " + strNum2 + " = " + (num1 - num2) + "\n";
        strNotification += strNum1 + " / " + strNum2 + " = " + (num1 / num2) + "\n";
        strNotification += strNum1 + " * " + strNum2 + " = " + (num1 * num2) + "\n";
        JOptionPane.showMessageDialog(null, strNotification, "Result", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }

}